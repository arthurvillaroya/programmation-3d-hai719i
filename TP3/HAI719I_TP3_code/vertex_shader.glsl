#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 vertices_position_modelspace;

//TODO create uniform transformations matrices Model View Projection
uniform mat4 model_mat;
uniform mat4 view_mat;
uniform mat4 proj_mat;

mat4 transform;
// Values that stay constant for the whole mesh.

void main(){

        // TODO : Output position of the vertex, in clip space : MVP * position
        transform = (proj_mat * view_mat * model_mat);
        gl_Position = transform * vec4(vertices_position_modelspace,1);

}

