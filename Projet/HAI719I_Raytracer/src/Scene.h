#ifndef SCENE_H
#define SCENE_H

#include <vector>
#include <string>
#include "Mesh.h"
#include "Sphere.h"
#include "Square.h"
#include "imageLoader.h"


#include <GL/glut.h>

const int SHADOW_SAMPLE_AMOUNT = 15;
const float SHADOW_BOX_SIZE = 0.35f;


enum LightType {
    LightType_Spherical,
    LightType_Quad
};


struct Light {
    Vec3 material;
    bool isInCamSpace;
    LightType type;

    Vec3 pos;
    float radius;

    Mesh quad;

    float powerCorrection;

    Light() : powerCorrection(1.0) {}

};

struct RaySceneIntersection{
    bool intersectionExists;
    unsigned int typeOfIntersectedObject;
    unsigned int objectIndex;
    float t;
    float u, v;
    RayTriangleIntersection rayMeshIntersection;
    RaySphereIntersection raySphereIntersection;
    RaySquareIntersection raySquareIntersection;
    RaySceneIntersection() : intersectionExists(false) , t(FLT_MAX) {}

    Vec3 interPoint;
    Vec3 normal;
    Vec3 tangeante;
    Vec3 bitangeante;
};



class Scene {
    std::vector< Mesh > meshes;
    std::vector< Sphere > spheres;
    std::vector< Square > squares;
    std::vector< Light > lights;

public:


    Scene() {
    }

    float calculDistance(Ray const & ray, float znear, float zfar) {

        RaySceneIntersection result;
        result.t = FLT_MAX;
        result.intersectionExists = false;
        
        for(unsigned long int i = 0; i < spheres.size(); i++){
            RaySphereIntersection interSphere = spheres[i].intersect(ray);

            if(interSphere.intersectionExists && interSphere.t < result.t && interSphere.t >= znear && interSphere.t <= zfar) {
                result.t = interSphere.t;
            }
        }

        for(unsigned long int i = 0; i < squares.size(); i++){
            RaySquareIntersection interSquare = squares[i].intersect(ray);

            if(interSquare.intersectionExists && interSquare.t < result.t && interSquare.t >= znear && interSquare.t <= zfar){
                result.t = interSquare.t;
            }
        }

        for(unsigned long int i = 0; i < meshes.size(); i++){
            RayTriangleIntersection interMesh = meshes[i].intersect(ray);

            if(interMesh.intersectionExists && interMesh.t < result.t && interMesh.t >= znear && interMesh.t <= zfar){
                result.t = interMesh.t;
            }
        }
        return result.t;
    }

    void draw() {
        // iterer sur l'ensemble des objets, et faire leur rendu :
        for( unsigned int It = 0 ; It < meshes.size() ; ++It ) {
            Mesh const & mesh = meshes[It];
            mesh.draw();
        }
        for( unsigned int It = 0 ; It < spheres.size() ; ++It ) {
            Sphere const & sphere = spheres[It];
            sphere.draw();
        }
        for( unsigned int It = 0 ; It < squares.size() ; ++It ) {
            Square const & square = squares[It];
            square.draw();
        }
    }

    RaySceneIntersection computeIntersection(Ray const & ray, float znear, float zfar) {

        RaySceneIntersection result;
        result.t = FLT_MAX;
        result.intersectionExists = false;
        
        for(unsigned long int i = 0; i < spheres.size(); i++){
            RaySphereIntersection interSphere = spheres[i].intersect(ray);

            if(interSphere.intersectionExists && interSphere.t < result.t && interSphere.t >= znear && interSphere.t <= zfar) {
                result.raySphereIntersection = interSphere;
                result.intersectionExists = true;
                result.objectIndex = i;
                result.typeOfIntersectedObject = 1;
                result.t = interSphere.t;
                result.interPoint = interSphere.intersection;
                result.normal = interSphere.normal;
            }
        }

        for(unsigned long int i = 0; i < squares.size(); i++){
            RaySquareIntersection interSquare = squares[i].intersect(ray);

            if(interSquare.intersectionExists && interSquare.t < result.t && interSquare.t >= znear && interSquare.t <= zfar){
                result.raySquareIntersection = interSquare;
                result.intersectionExists = true;
                result.objectIndex = i;
                result.typeOfIntersectedObject = 2;
                result.t = interSquare.t;
                result.interPoint = interSquare.intersection;
                result.normal = interSquare.normal;
                result.u = interSquare.u;
                result.u = interSquare.v;
                result.tangeante = interSquare.tangeante;
                result.bitangeante = interSquare.bitangeante;
            }
        }

        for(unsigned long int i = 0; i < meshes.size(); i++){
            RayTriangleIntersection interMesh = meshes[i].intersect(ray);

            if(interMesh.intersectionExists && interMesh.t < result.t && interMesh.t >= znear && interMesh.t <= zfar){
                result.rayMeshIntersection = interMesh;
                result.intersectionExists = true;
                result.objectIndex = i;
                result.typeOfIntersectedObject = 0;
                result.t = interMesh.t;
                result.interPoint = interMesh.intersection;
                result.normal = interMesh.normal;
            }
        }
        return result;
    }

    void phong(Ray ray, Vec3 & temp, RaySceneIntersection interScene, Light light, Material material, Vec3 & L){
        Vec3 Id = Vec3(0,0,0);
        Vec3 Is = Vec3(0,0,0);
        Vec3 Ia = Vec3(0,0,0);

        Vec3 N = interScene.normal;

        L.normalize();
        N.normalize();

        Vec3 R = 2 * std::max(Vec3::dot(N,L), 0.0f) * N - L;
        Vec3 V = ray.origin() - interScene.interPoint;

        R.normalize();
        V.normalize();
            
        Ia = material.ambient_material; 

        Id = Vec3::compProduct(light.material, material.diffuse_material);
        Id = Id * std::max(Vec3::dot(N,L), 0.0f);

        Is = Vec3::compProduct(light.material, material.specular_material);
        Is = Is * pow(std::max(Vec3::dot(R,V), 0.0f), material.shininess);

        temp = Id + Is + Ia;
        if(material.type == Material_Mirror){
            temp = Vec3::compProduct(temp, material.diffuse_material);
        } 
    }
    
    Vec3 refraction(Ray ray, RaySceneIntersection interScene, int NRemainingBounces, Material material){
        Vec3 normal = interScene.normal;
        normal.normalize();

        Vec3 rayDirection = ray.direction();
        rayDirection.normalize();

        float opacity = material.transparency;
        
        float angle = std::clamp(float(-1), float(1), Vec3::dot(rayDirection,normal));
        float etai = 1.0; float etat = material.index_medium;

        if(angle < 0){
            angle = -angle;
        }
        else{
            normal = -1 * normal;
            std::swap(etat,etai);
        }
        float eta = etai / etat;
        float k = 1 - eta * eta * (1 - angle * angle);

        Ray newRay;
        if(k < 0){
            newRay = Ray(interScene.interPoint, rayDirection);
        }
        else{
            Vec3 newDirection = -1*(eta * rayDirection + (eta * angle * sqrtf(k)) * normal);
            newDirection.normalize();
            newRay = Ray(interScene.interPoint, newDirection);
        }
        return opacity * (rayTraceRecursive(newRay , NRemainingBounces, 0.001, 10000));
    }

    Vec3 transperency(Ray ray, RaySceneIntersection interScene, int NRemainingBounces, Material material){
        Vec3 normal = interScene.normal;
        normal.normalize();

        Vec3 rayDirection = ray.direction();
        rayDirection.normalize();

        float opacity = material.transparency;
        float angle = Vec3::dot(rayDirection,normal);
        float index_medium = material.index_medium;
        Vec3 newDirection;

        float k = (index_medium * index_medium) * (angle * angle);

        if(k < 0){
            newDirection = rayDirection;
        }
        else{
            newDirection = (index_medium * rayDirection) + (index_medium * (angle + sqrt(k)) * normal);
            newDirection.normalize();
        }
        Ray newRay = Ray(interScene.interPoint, newDirection);

        return opacity * (rayTraceRecursive(newRay , NRemainingBounces, 0.001, 10000));
    }
    
    void ombreAdoucie(Vec3 & color, RaySceneIntersection interScene, Vec3 L){
        int cpt = 0;

        for(int i = 0; i < SHADOW_SAMPLE_AMOUNT; i++){
            Vec3 randomPoint = L;
            randomPoint[0] += static_cast <float> (rand()) / static_cast <float> (RAND_MAX/SHADOW_BOX_SIZE) - SHADOW_BOX_SIZE/2;
            randomPoint[2] += static_cast <float> (rand()) / static_cast <float> (RAND_MAX/SHADOW_BOX_SIZE) - SHADOW_BOX_SIZE/2;

            Ray rayon = Ray(interScene.interPoint, randomPoint);

            RaySceneIntersection inter = computeIntersection(rayon, 0.001, 10000);

            if(inter.intersectionExists){
                float distLum = randomPoint.length();
                float distInter = inter.t;

                if(distInter >= distLum){
                    cpt++;
                }
            }
            else{
                cpt++;
            }
        }

        color *= (cpt/SHADOW_SAMPLE_AMOUNT);
    }

    void ombre(Vec3 & color, RaySceneIntersection interScene, Vec3 L){
        Ray rayon = Ray(interScene.interPoint, L);

        RaySceneIntersection inter = computeIntersection(rayon, 0.0001, 100000);

        float distLum = L.length();
        float distInter = inter.t;

        if((distInter < distLum) && inter.intersectionExists){
            color = Vec3(0,0,0);
        }
    }

    void colorTexturePlane(Vec3 &color, RaySquareIntersection interSquare, Material mat){

        ppmLoader::ImageRGB texture = mat.texture;

        float u = interSquare.u;
        float v = 1 - interSquare.v;

        int Upos = u * (texture.w);
        int Vpos = v * (texture.h);

        if(texture.data.size() > 0){
            Vec3 textureColor = Vec3(texture.data[Upos + Vpos * texture.w].r/255.0, 
                                     texture.data[Upos + Vpos * texture.w].g/255.0, 
                                     texture.data[Upos + Vpos * texture.w].b/255.0); 
            color = textureColor;
        }
    }

    void modifNormalTexturePlane(RaySceneIntersection &interScene, RaySquareIntersection interSquare, Material mat){
        ppmLoader::ImageRGB texture = mat.normalTexture;

        float u = interSquare.u;
        float v = 1 - interSquare.v;

        int Upos = u * (texture.w);
        int Vpos = v * (texture.h);

        if(texture.data.size() > 0){
            Vec3 normalColor = Vec3(texture.data[Upos + Vpos * texture.w].r/255.0, 
                                     texture.data[Upos + Vpos * texture.w].g/255.0, 
                                     texture.data[Upos + Vpos * texture.w].b/255.0); 
            Vec3 newNormal = normalColor[2] * interSquare.normal + normalColor[0] * interSquare.tangeante + normalColor[1] * interSquare.bitangeante;
            //std::cout<<"Premiere normale : "<<interScene.normal[0]<<"||"<<interScene.normal[1]<<"||"<<interScene.normal[2]<<std::endl;
            interScene.normal = newNormal;
            interScene.normal.normalize();
            //std::cout<<"Transforme normale : "<<interScene.normal[0]<<"||"<<interScene.normal[1]<<"||"<<interScene.normal[2]<<std::endl;
        }
    }
    
    Vec3 rayTraceRecursive( Ray ray , int NRemainingBounces, float znear, float zfar) {
        Vec3 color = Vec3(0,0,0);
        Material mat;
        RaySceneIntersection interScene = computeIntersection(ray, znear, zfar);
        
        if(interScene.intersectionExists){
            if(interScene.typeOfIntersectedObject == 0){mat = meshes[interScene.objectIndex].material;}
            if(interScene.typeOfIntersectedObject == 1){mat = spheres[interScene.objectIndex].material;}
            if(interScene.typeOfIntersectedObject == 2){mat = squares[interScene.objectIndex].material;}

            if(interScene.typeOfIntersectedObject == 2){
                colorTexturePlane(color, interScene.raySquareIntersection, squares[interScene.objectIndex].material);
                modifNormalTexturePlane(interScene, interScene.raySquareIntersection, squares[interScene.objectIndex].material);
                //std::cout<<"test normale : "<<interScene.normal[0]<<"||"<<interScene.normal[1]<<"||"<<interScene.normal[2]<<std::endl;
            }
            
            if(NRemainingBounces > 0 && mat.type == Material_Mirror){
                Vec3 normal = interScene.normal;
                normal.normalize();
                Ray newRay = Ray(interScene.interPoint, ray.direction() - (2 * (Vec3::dot(ray.direction() ,normal))) * normal); 
                color += Vec3::compProduct(rayTraceRecursive(newRay , NRemainingBounces - 1, 0.001, 10000), mat.diffuse_material);
            }
            else if(mat.type == Material_Glass){
                color += Vec3::compProduct(refraction(ray, interScene, NRemainingBounces, mat), mat.diffuse_material);
            }
            else if(mat.type == Material_Transparency){
                color += Vec3::compProduct(transperency(ray, interScene, NRemainingBounces, mat), mat.diffuse_material);
            }
            else{
                Vec3 temp = color;
                for(unsigned long int i = 0; i < lights.size(); i++){
                    Vec3 L = lights[i].pos - interScene.interPoint;
                    phong(ray, temp, interScene, lights[i], mat, L);
                    ombreAdoucie(temp, interScene, L);
                    color += temp; 
                }
            }
        }

        return color;
    }


    Vec3 rayTrace( Ray const & rayStart, float znear, float zfar) {
        Vec3 color;
        color = rayTraceRecursive(rayStart, 2, znear, zfar);
        return color;
    }

    void setup_single_sphere() {
        meshes.clear();
        spheres.clear();
        squares.clear();
        lights.clear();

        {
            lights.resize( lights.size() + 1 );
            Light & light = lights[lights.size() - 1];
            light.pos = Vec3( 0.0, 0.0, 2 );
            light.radius = 4.5f;
            light.powerCorrection = 3.f;
            light.type = LightType_Spherical;
            light.material = Vec3(1,1,1);
            light.isInCamSpace = false;
        }

        {
            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.scale(Vec3(5., 5., 1.));
            s.translate(Vec3(0, 0, -1));
            s.build_arrays();
            s.material.diffuse_material = Vec3( 0.8,0.1,0.2 );
            s.material.specular_material = Vec3( 0.8,0.1,0.2 );
            s.material.shininess = 20;
        }

        {
            std::string filename("data/suzanne.off");
            meshes.resize( meshes.size() + 1 );
            Mesh & m = meshes[meshes.size() - 1];
            m.loadOFF(filename);
            m.scale(Vec3(0.8, 0.8, 0.8));
            m.translate(Vec3(0, 0, 0.0));
            m.build_arrays();
            m.material.diffuse_material = Vec3( 1.,0.5,1. );
            m.material.specular_material = Vec3( 1.,0.5,1. );
            m.material.shininess = 16;
        }
    }

    void setup_single_square() {
        meshes.clear();
        spheres.clear();
        squares.clear();
        lights.clear();

        {
            lights.resize( lights.size() + 1 );
            Light & light = lights[lights.size() - 1];
            light.pos = Vec3(-5,5,5);
            light.radius = 2.5f;
            light.powerCorrection = 2.f;
            light.type = LightType_Spherical;
            light.material = Vec3(1,1,1);
            light.isInCamSpace = false;
        }

        {
            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.build_arrays();
            s.material.diffuse_material = Vec3( 0.8,0.8,0.8 );
            s.material.specular_material = Vec3( 0.8,0.8,0.8 );
            s.material.shininess = 20;
        }
    }

    void setup_cornell_box(){
        meshes.clear();
        spheres.clear();
        squares.clear();
        lights.clear();

        {
            lights.resize(3);
            Light & light2 = lights[0];
            light2.pos = Vec3( -1, 1, 0 );
            light2.radius = 1.0f;
            light2.powerCorrection = 2.f;
            light2.type = LightType_Spherical;
            light2.material = Vec3(1,0,0);
            light2.isInCamSpace = false;
            Light & light = lights[1];
            light.pos = Vec3( 0, 1, 0 );
            light.radius = 1.0f;
            light.powerCorrection = 2.f;
            light.type = LightType_Spherical;
            light.material = Vec3(0,1,0);
            light.isInCamSpace = false;
            Light & light3 = lights[2];
            light3.pos = Vec3( 1, 1, 0 );
            light3.radius = 1.0f;
            light3.powerCorrection = 2.f;
            light3.type = LightType_Spherical;
            light3.material = Vec3(0,0,1);
            light3.isInCamSpace = false;
        }

       /*{
            lights.resize( lights.size() + 1 );
            Light & light = lights[lights.size() - 1];
            light.pos = Vec3(0,1,0);
            light.radius = 2.5f;
            light.powerCorrection = 2.f;
            light.type = LightType_Spherical;
            light.material = Vec3(1,1,1);
            light.isInCamSpace = false;
        }*/

        { //Back Wall
            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.scale(Vec3(2., 2., 1.));
            s.translate(Vec3(0., 0., -2.));
            s.build_arrays();
            //s.material.type = Material_Mirror;
            s.material.diffuse_material = Vec3( 1.0,1.0,1.0 );
            s.material.specular_material = Vec3( 1.0,1.0,1.0 );
            s.material.shininess = 16;
        }

        { //Left Wall

            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.scale(Vec3(2., 2., 1.));
            s.translate(Vec3(0., 0., -2.));
            s.rotate_y(90);
            s.build_arrays();
            s.material.diffuse_material = Vec3( 1.0,1.0,1.0 );
            s.material.specular_material = Vec3( 1.0,1.0,1.0 );
            s.material.shininess = 16;
        }

        { //Right Wall
            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.translate(Vec3(0., 0., -2.));
            s.scale(Vec3(2., 2., 1.));
            s.rotate_y(-90);
            s.build_arrays();
            s.material.type = Material_Mirror;
            s.material.diffuse_material = Vec3( 1.0,1.0,1.0 );
            //s.material.specular_material = Vec3( 1.0,1.0,1.0 );
            s.material.shininess = 16;
            //load_ppm(s.material.texture, "img/sphereTextures/s6.ppm");
            //load_ppm(s.material.normalTexture, "img/normalMaps/n2.ppm");
        }

        { //Floor
            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.translate(Vec3(0., 0., -2.));
            s.scale(Vec3(2., 2., 1.));
            s.rotate_x(-90);
            s.build_arrays();
            s.material.diffuse_material = Vec3( 1.0,1.0,1.0 );
            s.material.specular_material = Vec3( 1.0,1.0,1.0 );
            s.material.shininess = 16;
        }

        { //Obstacle
            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.translate(Vec3(0., 0., 1.15));
            s.scale(Vec3(2., 2., 1.));
            s.build_arrays();
            s.material.type = Material_Transparency;
            s.material.diffuse_material = Vec3( 1.0,1.0,1.0 );
            s.material.specular_material = Vec3( 1.0,1.0,1.0 );
            s.material.shininess = 16;
            s.material.transparency = 1.0;
            s.material.index_medium = 1.4;
            load_ppm(s.material.normalTexture, "img/normalMaps/n2.ppm");
        }

        { //Ceiling
            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.translate(Vec3(0., 0., -2.));
            s.scale(Vec3(2., 2., 1.));
            s.rotate_x(90);
            s.build_arrays();
            s.material.diffuse_material = Vec3( 1.0,1.0,1.0 );
            s.material.specular_material = Vec3( 1.0,1.0,1.0 );
            s.material.shininess = 16;
        }

       { //Front Wall
            squares.resize( squares.size() + 1 );
            Square & s = squares[squares.size() - 1];
            s.setQuad(Vec3(-1., -1., 0.), Vec3(1., 0, 0.), Vec3(0., 1, 0.), 2., 2.);
            s.translate(Vec3(0., 0., -2.));
            s.scale(Vec3(2., 2., 1.));
            s.rotate_y(180);
            s.build_arrays();
            //s.material.type = Material_Mirror;
            s.material.diffuse_material = Vec3( 1.0,1.0,1.0 );
            //s.material.specular_material = Vec3( 0.0,0.5,0.2 );
            s.material.shininess = 16;
        }


        {  //GLASS Sphere

            spheres.resize( spheres.size() + 1 );
            Sphere & s = spheres[spheres.size() - 1]; 
            s.m_center = Vec3(1., -1.0, 0.5);
            s.m_radius = 0.75f;
            s.build_arrays();
            s.material.type = Material_Transparency;
            s.material.diffuse_material = Vec3( 0.4,0.6,1. );
            s.material.specular_material = Vec3( 1.,1.,1. );
            s.material.shininess = 16;
            s.material.transparency = 1.0;
            s.material.index_medium = 1.4;
        }


        {//MIRRORED Sphere
            spheres.resize( spheres.size() + 1 );
            Sphere & s = spheres[spheres.size() - 1];
            s.m_center = Vec3(-1.25, -1.25, -0.5);
            s.m_radius = 0.75f;
            s.build_arrays();
            s.material.type = Material_Mirror;
            s.material.ambient_material = Vec3(0,0,0);
            s.material.diffuse_material = Vec3( 255/(float)255, 155/(float)255, 100/(float)255);
            s.material.specular_material = Vec3(  1.,1.,1. );
            s.material.shininess = 16;
            s.material.transparency = 1.0;
            s.material.index_medium = 1.3;
        }

        /*{//Mesh
            meshes.resize(meshes.size() + 1);
            Mesh &s = meshes[meshes.size() - 1];
            s.loadOFF("data/tetra.off");
            s.scale(Vec3(0.7, 0.7, 0.7));
            s.translate(Vec3(0, 0, 0));
            s.rotate_y(15);
            s.build_arrays();
            s.material.type = Material_Glass;
            s.material.diffuse_material = Vec3(1.0, 1.0, 1.0);
            s.material.specular_material = Vec3(0.2, 0.2, 0.2);
            s.material.shininess = 20;
            s.material.transparency = 1.0;
            s.material.index_medium = 1.3;
        }*/

    }

};



#endif
