#ifndef SQUARE_H
#define SQUARE_H
#include "Vec3.h"
#include <vector>
#include "Mesh.h"
#include <cmath>

#define EPSILON 0.0000001

struct RaySquareIntersection{
    bool intersectionExists;
    float t;
    float u,v;
    Vec3 intersection;
    Vec3 normal;
    Vec3 tangeante;
    Vec3 bitangeante; 
};


class Square : public Mesh {
public:
    Vec3 m_normal;
    Vec3 m_bottom_left;
    Vec3 m_right_vector;
    Vec3 m_up_vector;

    Square() : Mesh() {}
    Square(Vec3 const & bottomLeft , Vec3 const & rightVector , Vec3 const & upVector , float width=1. , float height=1. ,
           float uMin = 0.f , float uMax = 1.f , float vMin = 0.f , float vMax = 1.f) : Mesh() {
        setQuad(bottomLeft, rightVector, upVector, width, height, uMin, uMax, vMin, vMax);
    }

    Vec3 bottomLeft() const { return this->vertices[0].position; }
    Vec3 bottomRight() const { return this->vertices[1].position; }
    Vec3 upRight() const { return this->vertices[2].position; }
    Vec3 upLeft() const { return this->vertices[3].position; }
    Vec3 normal() const { return Vec3::cross((bottomRight() - bottomLeft()) , (upLeft() - bottomLeft())); }

    Vec3 tangeante() const {return bottomRight() - bottomLeft(); }
    Vec3 bitangeante() const {return Vec3::cross(normal(), tangeante()); }


    void setQuad( Vec3 const & bottomLeft , Vec3 const & rightVector , Vec3 const & upVector , float width=1. , float height=1. ,
                  float uMin = 0.f , float uMax = 1.f , float vMin = 0.f , float vMax = 1.f) {
        m_right_vector = rightVector;
        m_up_vector = upVector;
        m_normal = Vec3::cross(rightVector , upVector);
        m_bottom_left = bottomLeft;

        m_normal.normalize();
        m_right_vector.normalize();
        m_up_vector.normalize();

        m_right_vector = m_right_vector*width;
        m_up_vector = m_up_vector*height;

        vertices.clear();
        vertices.resize(4);
        vertices[0].position = bottomLeft;                                      vertices[0].u = uMin; vertices[0].v = vMin;
        vertices[1].position = bottomLeft + m_right_vector;                     vertices[1].u = uMax; vertices[1].v = vMin;
        vertices[2].position = bottomLeft + m_right_vector + m_up_vector;       vertices[2].u = uMax; vertices[2].v = vMax;
        vertices[3].position = bottomLeft + m_up_vector;                        vertices[3].u = uMin; vertices[3].v = vMax;
        vertices[0].normal = vertices[1].normal = vertices[2].normal = vertices[3].normal = m_normal;
        triangles.clear();
        triangles.resize(2);
        triangles[0][0] = 0;
        triangles[0][1] = 1;
        triangles[0][2] = 2;
        triangles[1][0] = 0;
        triangles[1][1] = 2;
        triangles[1][2] = 3;
    }

    Vec3 getUV(const Vec3 &intersection) const{

        Vec3 largeur = bottomRight() - bottomLeft();
        Vec3 hauteur = upLeft() - bottomLeft();

        Vec3 bottomToPoint = intersection - bottomLeft();
        
        float u = Vec3::dot(bottomToPoint,largeur) / largeur.squareLength();
        float v = Vec3::dot(bottomToPoint,hauteur) / hauteur.squareLength();

        return Vec3(u,v,0);
    }

    bool estDansCarre(const Vec3 &point) const{

        Vec3 largeur = bottomRight() - bottomLeft();
        Vec3 hauteur = upLeft() - bottomLeft();

        Vec3 bottomToPoint = point - bottomLeft();

        float projOnLargeur = Vec3::dot(largeur,bottomToPoint)/largeur.length();
        float projOnHauteur = Vec3::dot(hauteur,bottomToPoint)/hauteur.length();

        bool estDansX = projOnLargeur >= 0 && projOnLargeur <= largeur.length();
        bool estDansY = projOnHauteur >= 0 && projOnHauteur <= hauteur.length();

        return estDansX && estDansY;
    }

    RaySquareIntersection intersect(const Ray &ray) const {
        RaySquareIntersection intersection;

        Vec3 direction = ray.direction();
        Vec3 origin = ray.origin();

        float D = Vec3::dot(bottomRight(), normal());
        float on = Vec3::dot(origin, normal());
        float dn = Vec3::dot(direction, normal()); 
        
        if(dn <= EPSILON && dn >= -(EPSILON)){ 
            intersection.intersectionExists = false;
            return intersection;
        }
        
        float t0 = (D - on)/dn;
        
        if(t0 < 0){
            intersection.intersectionExists = false;
        }
        else{
            Vec3 res = origin + (direction*t0);
            if(!estDansCarre(res)){
                intersection.intersectionExists = false;
            }
            else{
                Vec3 uvO = getUV(res);
                intersection.u = uvO[0];
                intersection.v = uvO[1];   
                intersection.intersectionExists = true;   
                intersection.t = t0;
                intersection.intersection = res;
                intersection.normal = normal();
                intersection.normal.normalize();
                intersection.tangeante = tangeante();
                intersection.tangeante.normalize();
                intersection.bitangeante = bitangeante();
                intersection.bitangeante.normalize();
            }
        }

        return intersection;
    }
};
#endif // SQUARE_H
