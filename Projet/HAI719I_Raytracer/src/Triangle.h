#ifndef TRIANGLE_H
#define TRIANGLE_H
#include "Vec3.h"
#include "Ray.h"
#include "Plane.h"

#define EPSILON 0.000001

struct RayTriangleIntersection{
    bool intersectionExists;
    float t;
    float w0,w1,w2;
    unsigned int tIndex;
    Vec3 intersection;
    Vec3 normal;
};

class Triangle {
private:
    Vec3 m_c[3] , m_normal;
    float area;
public:
    Triangle() {}
    Triangle( Vec3 const & c0 , Vec3 const & c1 , Vec3 const & c2 ) {
        m_c[0] = c0;
        m_c[1] = c1;
        m_c[2] = c2;
        updateAreaAndNormal();
    }
    void updateAreaAndNormal() {
        Vec3 nNotNormalized = Vec3::cross( m_c[1] - m_c[0] , m_c[2] - m_c[0] );
        float norm = nNotNormalized.length();
        m_normal = nNotNormalized / norm;
        area = norm / 2.f;
    }
    void setC0( Vec3 const & c0 ) { m_c[0] = c0; } // remember to update the area and normal afterwards!
    void setC1( Vec3 const & c1 ) { m_c[1] = c1; } // remember to update the area and normal afterwards!
    void setC2( Vec3 const & c2 ) { m_c[2] = c2; } // remember to update the area and normal afterwards!
    Vec3 const & normal() const { return m_normal; }

    Vec3 projectOnSupportPlane( Vec3 const & p ) const {
        Vec3 result;

        return result;
    }

    float squareDistanceToSupportPlane( Vec3 const & p ) const {
        float result;
        //TODO completer
        return result;
    }

    float distanceToSupportPlane( Vec3 const & p ) const { return sqrt( squareDistanceToSupportPlane(p) ); }

    bool isParallelTo( Line const & L ) const {
        bool result;
        //TODO completer
        return result;
    }

    Vec3 getIntersectionPointWithSupportPlane( Line const & L ) const {
        // you should check first that the line is not parallel to the plane!
        Vec3 result;
        //TODO completer
        return result;
    }

    void computeBarycentricCoordinates( Vec3 const & p , float & u0 , float & u1 , float & u2 ) const {
        //TODO Complete
    }

    RayTriangleIntersection getIntersection( Ray const & ray ) const {
        RayTriangleIntersection intersection;
        intersection.intersectionExists = false;
        
        float NdotRayDirection = Vec3::dot(normal(),ray.direction());
        if(NdotRayDirection <= EPSILON && NdotRayDirection >= -(EPSILON)){ 
            return intersection;
        }

        float d = -(Vec3::dot(normal(),m_c[0]));

        float tTemp = -(d + Vec3::dot(normal(),ray.origin()))/NdotRayDirection;

        Vec3 P = ray.origin() + tTemp * ray.direction();
        Vec3 C;

        Vec3 AB = m_c[1] - m_c[0]; //inutile
        Vec3 vp0 = P - m_c[0];
        C = Vec3::cross(AB, vp0);
        if(Vec3::dot(normal(),C) < 0){return intersection;}

        Vec3 BC = m_c[2] - m_c[1];
        Vec3 vp1 = P - m_c[1];
        C = Vec3::cross(BC, vp1);
        if(Vec3::dot(normal(),C) < 0){return intersection;}

        Vec3 CA = m_c[0] - m_c[2];
        Vec3 vp2 = P - m_c[2];
        C = Vec3::cross(CA, vp2);
        if(Vec3::dot(normal(),C) < 0){return intersection;} 

        intersection.t = tTemp;
        intersection.normal = normal();
        intersection.intersection = P;
        intersection.intersectionExists = true;
        return intersection;
    }
};
#endif
