#version 330 core

in vec3 o_positionWorld;
in vec3 o_normalWorld;
in vec2 o_uv0;
in vec3 o_tangentWorld;
out vec4 FragColor;

uniform vec4 color;
uniform sampler2D colorTexture;
uniform sampler2D normalTexture;
uniform sampler2D emissivTexture;
uniform sampler2D metalTexture;
uniform vec3 cameraPosition;

vec2 inverO_uv0 = vec2(o_uv0.x,1.0-o_uv0.y);

struct Light{
    vec3 pos;
    vec4 rgb;
};



vec3 getNewNormal(vec2 inverO_uv0){
    vec3 normal = texture(normalTexture, inverO_uv0).rgb;
    return (normal.b * o_normalWorld) + (normal.r * o_tangentWorld) + (normal.g * cross(o_tangentWorld, o_normalWorld)); 
}

vec4 phong(vec2 inverO_uv0){
    Light light; light.pos = vec3(5,5,5); light.rgb = vec4(5,5,5,5);
    vec4 Id = vec4(0,0,0,0);
    vec4 Is = vec4(0,0,0,0);

    float diffuse = 0;
    float specular = 0;
    vec3 metal = texture(metalTexture, inverO_uv0).rgb;
    if(metal.r > 0.75){
        specular = 0.8;
        diffuse = 0.3;
    }
    if(metal.g > 0.5){
        specular = 0.4;
        diffuse = 0.8;
    }
    if(metal.b > 0.5){
        specular = 0.8;
        diffuse = 0.8;
    }
    vec3 N = normalize(getNewNormal(inverO_uv0));
    vec3 L = normalize(light.pos - o_positionWorld);
    vec3 R = normalize(reflect(L,N));
    vec3 V = normalize(cameraPosition - o_positionWorld);

    Id = light.rgb * diffuse;
    Id = Id * max(dot(L,N), 0);

    Is = light.rgb * specular;
    Is = Is * pow(max(dot(R,V), 0), 0.6);

    return (Is + Id)/2.0; 
}

void main() {
    FragColor = (texture(colorTexture, inverO_uv0).rgba * color * phong(inverO_uv0)) + texture(emissivTexture, inverO_uv0).rgba;
}
