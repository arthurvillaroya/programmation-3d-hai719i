#ifndef LIGHT_H_DEFINED
#define LIGHT_H_DEFINED
#include <GL/glew.h>
#include <GL/glut.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

struct Light {

    glm::vec3 pos;
    glm::vec3 color;

    Light();
    Light(glm::vec3 pos, glm::vec3 color);
};

#endif