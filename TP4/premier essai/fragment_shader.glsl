#version 330 core

// Output data
out vec4 FragColor;
// Ajouter une variable interpolée o_color venant du vertex shader
in vec4 o_color;
in vec2 uv;
uniform sampler2D tex;
void main()
{

    // Mettre à jour la couleur avec la variable interpolée venant du vertex shader
    FragColor = texture(tex, uv);// Output color = red

}
