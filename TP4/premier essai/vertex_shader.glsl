#version 330 core
//A faire
// ajouter une variable uniform pour tous les sommets de type float permettant la mise à l'échelle
// ajouter une variable uniform pour tous les sommets de type vec3 permettant d'appliquer une translation au modèle
// i.e. a appliquer sur tous les sommets
//---------

//---------
// ajouter une variable o_color de type vec3 interpolée a envoyer au fragment shader
uniform float scale;
uniform vec3 translate;
// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 vertexPosition_modelspace;
// A faire : ajouter un attribut de sommet color, contenant les couleurs pour chaque sommet à ( location = 1 )
layout(location = 1) in vec3 vertexColors; 

layout(location = 2) in vec2 uvText;

out vec4 o_color;
out vec2 uv;

void main(){

    //Mettre à jour ce code pour appliquer la translation et la mise à l'échelle
    gl_Position = vec4(vertexPosition_modelspace*scale+translate,1);

    //Assigner la normale à la variable interpolée color
    o_color = vec4(vertexColors.x, vertexColors.y, vertexColors.z, 1);
    uv = uvText;
}

